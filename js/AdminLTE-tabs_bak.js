


var tabs = {};


tabs.$tabs_title = $("#tabs_title");
tabs.$tabs_content = $("#tabs_content");
tabs.$tabs_slimScroll = $("#tabs_slimScroll");
tabs.nextId = 0;

// 设置滚动区宽度
tabs.$tabs_slimScroll[0].style.right = Math.ceil($("#app_menu_left").width()) + 25 + "px";

tabs.open = function (title, url) {
    // 如果是低分辨率，则在当先已显示的iframe打开新的页面
    if (tabs.isLowResolution()) {
        // 获取已显示的iframe
        var $iframe =  tabs.getCurrentIframe();

        // 存在的话就在这个iframe里打开，不存在的话就先打开一个
        if ($iframe.length !== 0) {
            $iframe.attr("src", url);
            return;
        }

    }


    ++tabs.nextId;

    var str1 = '<li id="tabs_title_' + tabs.nextId + '"><a href="#tabs_content_' + tabs.nextId + '" data-toggle="tab">' + title + '<i class="fa fa-remove tabs-title-close" onclick="tabs.close(' + tabs.nextId +')"></i></a></li>';

    var str2 = '<div class="tab-pane" id="tabs_content_' + tabs.nextId + '">';
    str2 += '<iframe src="' + url + '"></iframe>';
    str2 += '</div>';



    tabs.$tabs_title.append(str1);
    tabs.$tabs_content.append(str2);
    tabs.select(tabs.nextId);
};

tabs.close = function (id) {
    $("#tabs_title_" + id).remove();
    // $("#tabs_title_close_" + id).remove();
    $("#tabs_content_" + id).remove();
};


tabs.unSelect = function () {
    tabs.$tabs_title.find(".active").removeClass("active");
    tabs.$tabs_content.find(".active").removeClass("active");
};

tabs.select = function (id) {
    tabs.unSelect();

    var tabsMenu = $("#tabs_title_" + id);
    tabsMenu.addClass("active");
    $("#tabs_content_" + id).addClass("active");

    tabs.$tabs_slimScroll[0].scrollLeft = tabsMenu[0].offsetLeft;
};

/**
 * 获取当前页面显示的iframe
 * @returns {*|{}}
 */
tabs.getCurrentIframe = function () {
    return tabs.$tabs_content.find(".active").find("iframe");
};

/**
 * 当前处于低分辨率下
 */
tabs.isLowResolution = function () {
    // 只要判断$tabs_title是不是隐藏的就行了
    return tabs.$tabs_title.is(":hidden");
};


tabs.$tabs_slimScroll.on("mousewheel DOMMouseScroll", function (e) {
    var delta = (e.originalEvent.wheelDelta && (e.originalEvent.wheelDelta > 0 ? 1 : -1)) ||  // chrome & ie
        (e.originalEvent.detail && (e.originalEvent.detail > 0 ? -1 : 1));              // firefox
    if (delta > 0) {
        // 向上滚
        tabs.$tabs_slimScroll[0].scrollLeft = tabs.$tabs_slimScroll[0].scrollLeft - 20;
    } else if (delta < 0) {
        // 向下滚
        tabs.$tabs_slimScroll[0].scrollLeft = tabs.$tabs_slimScroll[0].scrollLeft + 20;
    }
});

tabs.next = function () {
    tabs.$tabs_slimScroll[0].scrollLeft = tabs.$tabs_slimScroll[0].scrollLeft - 100;
};

tabs.last = function () {
    tabs.$tabs_slimScroll[0].scrollLeft = tabs.$tabs_slimScroll[0].scrollLeft + 100;
};