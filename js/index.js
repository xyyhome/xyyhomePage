// 首页模板


var app = {};






// =========================================
// tabs 标签页
// =========================================
app.tabs = {};// 标签页
app.tabs.$titleZone = null;// 标签页标题区域
app.tabs.$contentZone = null;// 标签页内容区域
app.tabs.$scrollZone = null;// 标签页标题区域(滚动区)
app.tabs.$titlesZone = null;// 标签页标题区域(标题区)

/**
 * 初始化菜单
 * @param titlesId 标题区域的jq选择器
 * @param contentsId 内容区域的jq选择器
 */
app.tabs.init = function (titlesId, contentsId) {
    app.tabs.$titleZone = $(titlesId);
    app.tabs.$contentZone = $(contentsId);

    app.tabs.$titleZone.empty();
    app.tabs.$titleZone.append(
        '<a class="tabs_title_left pull-left" href="javascript:app.tabs.scrollTo(-200)"><i class="fa fa-chevron-left"></i></a>' +
        '<div class="tabs_title_slimScroll"></div>' +
        '<a class="tabs_title_right pull-right" href="javascript:app.tabs.scrollTo(200)"><i class="fa fa-chevron-right"></i></a>');

    app.tabs.$scrollZone = app.tabs.$titleZone.find("div");
    app.tabs.$scrollZone.append('<ul class="nav nav-tabs tabs_title_menu"></ul>');

    app.tabs.$titlesZone = app.tabs.$scrollZone.find("ul");

    // 绑定滚轮事件
    app.tabs.$scrollZone.on("mousewheel DOMMouseScroll", function (e) {
        var delta = (e.originalEvent.wheelDelta && (e.originalEvent.wheelDelta > 0 ? 1 : -1)) ||  // chrome & ie
            (e.originalEvent.detail && (e.originalEvent.detail > 0 ? -1 : 1));              // firefox
        if (delta > 0) {
            // 向上滚
            app.tabs.scrollTo(-20);
        } else if (delta < 0) {
            // 向下滚
            app.tabs.scrollTo(20);
        }
    });
};

/**
 * 打开新页面
 * @param id 页面id
 * @param title 标题
 * @param url 地址
 * @param hasCloseButton 是否显示关闭按钮
 */
app.tabs.open = function (id, title, url, hasCloseButton) {
    // 无id直接跳过
    if (!id)
        return false;

    // 如果指定id的标签页存在的话就直接打开
    if (app.tabs.select(id))
        return;

    // 不考虑低分辨率了
    // 如果是低分辨率，则在当先已显示的iframe打开新的页面
    // if (app.isLowResolution) {
    //     // 获取已显示的iframe
    //     var $iframe =  app.tabs.$contentZone.find(".active iframe");
    //     var $span = app.tabs.$titlesZone.find(".active span");
    //     // 存在的话就在这个iframe里打开，不存在的话就先打开一个
    //     if ($iframe.length !== 0) {
    //         $iframe.attr("src", url);
    //         $span.text(title);
    //         return;
    //     }
    // }

    hasCloseButton = hasCloseButton === undefined || hasCloseButton === true;// 默认显示关闭按钮
    var noClose = "";
    if (!hasCloseButton)// 不显示关闭按钮时加上呲样式
        noClose = "noClose";

    // 正常分辨率
    var str1 =
        '<li id="tabs_title_' + id + '">' +
        '<a href="#tabs_content_' + id + '" class="' + noClose + '" data-toggle="tab"><span>' + title + '</span>';
    if (hasCloseButton)// 显示关闭按钮
        str1 += '<i class="fa fa-remove tabs-title-close" onclick="app.tabs.close(\'' + id +'\')"></i>';
    str1 += '</a></li>';
    var str2 = '<div class="tab-pane fade" id="tabs_content_' + id + '">';
    str2 += '<iframe src="' + url + '"></iframe>';
    str2 += '</div>';
    app.tabs.$titlesZone.append(str1);
    app.tabs.$contentZone.append(str2);

    // 打开自动选中
    app.tabs.select(id);
};

/**
 * 选中指定标签页，标签页不存在则返回false，存在返回true
 * @param id
 */
app.tabs.select = function (id) {
    return app.tabs.selectByJqObj($("#tabs_title_" + id));
};

/**
 * 通过jq对象选择标签页
 * @param tabsMenu jq对象（li的jq对象）
 * @returns {boolean}
 */
app.tabs.selectByJqObj = function (tabsMenu) {
    if (tabsMenu.length === 0)
        return false;// 不存在返回false
    tabsMenu.find("a").tab('show');// 调用bootstrap的方法显示指定标签页
    app.tabs.$scrollZone[0].scrollLeft = tabsMenu[0].offsetLeft;// 滚动条滚动到最后
    return true;
};

/**
 * 关闭指定标签页
 * @param id
 */
app.tabs.close = function (id) {
    // 获取待删除的标签页li
    var thisLi = $("#tabs_title_" + id);

    // 在当要关闭的标签页不是已显示的标签页时，才在关闭后切换标签页
    var nextLi = null;
    if (thisLi.hasClass("active")) {
        // 获取下一个，下一个不存在则获取上一个
        nextLi = thisLi.next();
        if (nextLi.length === 0) {
            nextLi = thisLi.prev();
        }
    }


    // 删除
    thisLi.remove();
    $("#tabs_content_" + id).remove();

    // 存在下一个则选中
    if (nextLi !== null && nextLi.length !== 0) {
        app.tabs.selectByJqObj(nextLi);
    }
};
/**
 * 向后滚动
 * @param range 距离
 */
app.tabs.scrollTo = function (range) {
    app.tabs.$scrollZone[0].scrollLeft = app.tabs.$scrollZone[0].scrollLeft + range;
};



// =========================================
// menus 菜单
// =========================================
app.menus = {};// 菜单

app.menus.menuType = 1;// 1=一字型菜单,2=广字型菜单




// menus菜单
var menus = {};
menus.subMenuStr = "subMenu";


// 菜单数据模型
menus.defaultData = {
    id:"菜单id",
    parent:"父菜单id",
    icon:"图标class",
    title:"标题",
    url:"url",
    subMenu:""//menus.subMenuStr
};
menus.$menu_zone = $("#menu_zone");// 菜单的容器


/**
 * 清空菜单
 * @param jqObject
 */
menus.clear = function (jqObject) {
    jqObject.empty();
};


// 通过url加载菜单
menus.loadByUrl = function (url) {

};
// 通过数据加载菜单
menus.loadByData = function (jqObject,menuData) {
    menuData = menus.parseMenuDate(menuData);
    var str = menus.dataAppend(menuData);
    jqObject.append(str);
    return jqObject.metisMenu();
};



/**
 * metisMenuScript.dataAppend中的递归，用于拼接
 */
menus.dataAppend = function (data) {
    // 拼菜单文本
    var str = "";
    for (var i = 0, temp; i < data.length; ++i) {
        temp = data[i];
        str += '<li>';
        str += '<a href="javascript:';
        if (temp.url) {
            str += 'app.tabs.open(\'';
            if (temp.id)
                str += temp.id;
            str += '\',\'';
            if (temp.title)
                str += temp.title;
            str += '\',\'' + temp.url + '\'';
            str += ');"';
        } else
            str += 'void(0)"';
        if (temp.subMenu)
            str += ' class="has-arrow"';
        str += '>';
        if (temp.icon)
            str += '<i class="' + temp.icon + '"></i>';
        if (temp.title)
            str += "<span class='metismenu-bigonly'>" + temp.title + "</span>";
        str += '</a>';

        if (temp.subMenu) {
            // 有下级菜单
            str += '<ul>';
            str += menus.dataAppend(temp.subMenu);
            str += '</ul>';
        }
        str += '</li>';
    }
    return str;
};







/**
 * 解析菜单数据，一级转多级
 * @param data
 * @param subMenuStr
 * @returns {Array}
 */
menus.parseMenuDate =  function(data, subMenuStr) {
    function getmenuData(arr, str, ziStr) {
        var temp = null;
        for (var i = 0; i < arr.length; ++i) {
            if (arr[i].id === str)
                return arr[i];
            if (arr[i][ziStr] && arr[i][ziStr].length !== 0)
                temp = getmenuData(arr[i][ziStr], str, ziStr);
            if (temp)
                return temp;
        }
    }
    var menuData = [];
    if (!subMenuStr) subMenuStr = "subMenu";
    for (var i = 0; i < data.length; ++i) {
        var temp = data[i];
        if (temp.parent) {
            var foo = getmenuData(menuData, temp.parent, subMenuStr);
            if (!foo) {
                foo = getmenuData(data, temp.parent, subMenuStr);
                if (!foo)
                    continue;
            }
            if (!foo[subMenuStr])
                foo[subMenuStr] = [];
            foo[subMenuStr].push(temp);
        } else
            menuData.push(temp);
    }
    return menuData;
};